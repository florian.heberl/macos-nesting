#!/bin/zsh

set -euo pipefail

# Load zshrc (we may be in a non-login, non-interactive shell here, since this
# is launched by packer as an executable shell script)

# shellcheck disable=SC1091
source /etc/zprofile
# shellcheck disable=SC1090
source ~/.zshrc

set -x

# Test tart image usage
tart clone "$(tart list -q | tail -n1)" init

tart run init --no-graphics &
TART_PID=$!

tart ip init --wait 3600

kill ${TART_PID}

tart delete init
tart prune --older-than 0
