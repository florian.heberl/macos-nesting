#!/bin/zsh

set -euo pipefail

# Load zshrc (we may be in a non-login, non-interactive shell here, since this
# is launched by packer as an executable shell script)

# shellcheck disable=SC1091
source /etc/zprofile
# shellcheck disable=SC1090
source ~/.zshrc

set -x

# resize disk. new disk size is only visible after reboot
CONTAINER_ID=$(diskutil list physical external | awk '/Apple_APFS/ {print $7}')
DISK_ID=$(echo "${CONTAINER_ID}" | cut -d's' -f1-2)

echo 'y' | sudo diskutil repairDisk "${DISK_ID}"
sudo diskutil apfs resizeContainer "${CONTAINER_ID}" 0

# Setup language
echo export LANG=en_US.UTF-8 >> ~/.zshrc
echo export LC_ALL=en_US.UTF-8 >> ~/.zshrc

# Disable spotlight
sudo mdutil -a -i off

# Update brew
brew update

# Install coreutils
brew install coreutils
