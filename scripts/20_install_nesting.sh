#!/bin/zsh

set -euo pipefail

# Load zshrc (we may be in a non-login, non-interactive shell here, since this
# is launched by packer as an executable shell script)

# shellcheck disable=SC1091
source /etc/zprofile
# shellcheck disable=SC1090
source ~/.zshrc

set -x

# Install nesting
echo "Installing nesting"

if [ "${NESTING_VERSION}" = "" ]; then
  echo "NESTING_VERSION is required"
  exit 1
fi

curl -sLo nesting "https://gitlab.com/gitlab-org/fleeting/nesting/-/releases/${NESTING_VERSION}/downloads/nesting-darwin-arm64"
chmod +x nesting

sha256sum -c /Users/ec2-user/nesting.checksum
rm /Users/ec2-user/nesting.checksum

echo '{}' >> /Users/ec2-user/nesting.json

sudo mkdir -p /usr/local/bin

sudo mv /Users/ec2-user/nesting /usr/local/bin/
sudo mv /Users/ec2-user/nesting.plist /Library/LaunchDaemons/nesting.plist
