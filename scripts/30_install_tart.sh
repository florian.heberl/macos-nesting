#!/bin/zsh

set -euo pipefail

# Load zshrc (we may be in a non-login, non-interactive shell here, since this
# is launched by packer as an executable shell script)

# shellcheck disable=SC1091
source /etc/zprofile
# shellcheck disable=SC1090
source ~/.zshrc

set -x

# Install tart
echo "Installing tart"

brew install cirruslabs/cli/tart

# Clone tart images from ECR
echo "Cloning base image"

while read -r imageName alias; do
  tart clone "${imageName}" "${alias}"
done < /Users/ec2-user/images.list

rm /Users/ec2-user/images.list
