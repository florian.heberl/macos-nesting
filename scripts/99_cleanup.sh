#!/bin/zsh

set -euo pipefail

# Load zshrc (we may be in a non-login, non-interactive shell here, since this
# is launched by packer as an executable shell script)

# shellcheck disable=SC1091
source /etc/zprofile
# shellcheck disable=SC1090
source ~/.zshrc

set -x

# Remove ec2-macos-init history so everything runs again
# when launching an instance from this AMI
sudo rm -rf /usr/local/aws/ec2-macos-init/instances/*
cat /dev/null > .ssh/authorized_keys
