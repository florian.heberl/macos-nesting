#!/bin/zsh

set -euo pipefail

# Load zshrc (we may be in a non-login, non-interactive shell here, since this
# is launched by packer as an executable shell script)

# shellcheck disable=SC1091
source /etc/zprofile
# shellcheck disable=SC1090
source ~/.zshrc

set -x

# Setup ecr credential helper, tart can use this even though it is initially for docker
brew install docker-credential-helper-ecr

mkdir -p ~/.docker
echo '{"credHelpers":{"915502504722.dkr.ecr.eu-west-1.amazonaws.com":"ecr-login"}}' > ~/.docker/config.json
