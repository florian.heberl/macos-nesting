# Credentials should be set externally!
AWS_REGION ?= unknown
AWS_ACCESS_KEY_ID ?= unknown
AWS_SECRET_ACCESS_KEY ?= unknown

#
# Images are currently built in our Sandbox account
# A manual job will publish them to other accounts like the one
# where we host our staging or production environments
#
# 251165465090 is the SaaS MacOS Staging account
#
PUBLISH_ACCOUNT_ID ?= 251165465090

# BUILD_ENVIRONMENT defines where the image will be built.
# Local execution will use the verify-runner sandbox account,
# while the CI execution will use the SaaS MacOS Staging runners
# account.
BUILD_ENVIRONMENT ?= sandbox

# MANIFEST_FILE contains the machine usable output of `packer build`
# execution. It can be used to read the ID of the built AMI.
MANIFEST_FILE ?= manifest.json

.PHONY: packer-init
packer-init:
	packer init main.pkr.hcl

.PHONY: packer-validate
packer-validate: packer-init
	packer validate \
		-var-file main.pkrvars.hcl \
		-var-file env-$(BUILD_ENVIRONMENT).pkrvars.hcl \
		-var "manifest_file=$(MANIFEST_FILE)" \
		main.pkr.hcl

.PHONY: packer-fmt
packer-fmt: packer-init
	packer fmt main.pkr.hcl
	git --no-pager diff --compact-summary --exit-code -- main.pkr.hcl

.PHONY: packer-build
packer-build: packer-init .check_credentials
	packer build \
		-timestamp-ui \
		-var-file main.pkrvars.hcl \
		-var-file env-$(BUILD_ENVIRONMENT).pkrvars.hcl \
		-var "manifest_file=$(MANIFEST_FILE)" \
		main.pkr.hcl

.PHONY: packer-publish
packer-publish: AMI_ID ?= $(shell jq -r '.builds[-1].artifact_id | split(":")[1]' "$(MANIFEST_FILE)")
packer-publish: .check_credentials .check_region
	aws --output json \
		ec2 modify-image-attribute \
		--image-id "$(AMI_ID)" \
		--launch-permission "Add=[{UserId=$(PUBLISH_ACCOUNT_ID)}]"

.PHONY: .check_credentials
.check_credentials:
ifeq ($(AWS_ACCESS_KEY_ID), unknown)
	@echo "AWS_ACCESS_KEY_ID has to be defined"
	@exit 1
endif
ifeq ($(AWS_SECRET_ACCESS_KEY), unknown)
	@echo "AWS_SECRET_ACCESS_KEY has to be defined"
	@exit 1
endif
	@echo "Credentials are set properly"

.PHONY: .check_region
.check_region:
ifeq ($(AWS_REGION), unknown)
	@echo "AWS_REGION has to be defined"
	@exit 1
endif
