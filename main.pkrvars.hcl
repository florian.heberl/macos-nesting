nesting = {
  version  = "v0.1.0",
  checksum = "e2070c74b388c7aa462d5d3b8e07294fb31d49a9827393cfbdfaed5e099e167f",
}

tart_images = {
  "macos-12-xcode-13" = "915502504722.dkr.ecr.eu-west-1.amazonaws.com/macos/release:macos-12-xcode-13-202210140817",
  "macos-12-xcode-14" = "915502504722.dkr.ecr.eu-west-1.amazonaws.com/macos/release:macos-12-xcode-14-202210140817",
}
