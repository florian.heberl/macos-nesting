packer {
  required_version = "~> 1.8.3"

  required_plugins {
    amazon = {
      version = ">= 1.1.4"
      source  = "github.com/hashicorp/amazon"
    }
  }
}

#############
# Variables #
#############

variable "region" {
  type = string

  description = "AWS region in which to build the image"
}

variable "host_resource_group_arn" {
  type = string

  description = "ARN of the host resource group used to spin-up AWS mac2.metal instances"
}

variable "license_configuration_arn" {
  type = string

  description = "ARN of the license configuration used to spin-up AWS mac2.metal isntaces"
}

variable "nesting" {
  type = map(string)

  description = "Hash containing version and checksum values of nesting release to be installed on the created AMI"
}

variable "tart_images" {
  type = map(string)

  description = "Hash containing aliases and ECR URLs for Tart images to be installed on the created AMI"
}

variable "manifest_file" {
  type = string

  description = "Name of the file that will hold manifest with details of built AMI"
}

#########################
# AMI source definition #
#########################

source "amazon-ebs" "macos" {
  ami_name = format("fleeting-${source.name}-%s", formatdate("YYYYMMDDhhmmssZ", timestamp()))

  instance_type = "mac2.metal"
  region        = var.region

  ena_support   = true
  ebs_optimized = true

  temporary_security_group_source_public_ip = true

  ssh_username = "ec2-user"
  // AWS says time to readiness can be up to 40min for M1 instances,
  // 15min for Intel. https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ec2-mac-instances.html
  ssh_timeout = "1h"

  launch_block_device_mappings {
    device_name = "/dev/sda1"
    volume_size = 500 # Each macOS image is around 50GB when uncompressed
  }

  placement {
    host_resource_group_arn = var.host_resource_group_arn
    tenancy                 = "host"
  }

  license_specifications {
    license_configuration_request {
      license_configuration_arn = var.license_configuration_arn
    }
  }

  # overrides
  source_ami_filter {
    filters = {
      name                = "amzn-ec2-macos-12.*"
      root-device-type    = "ebs"
      virtualization-type = "hvm"
      architecture        = "arm64_mac"
    }
    most_recent = true
    owners = [
      "amazon",
    ]
  }

  run_tags = {
    Name = "fleeting-${source.name}"
  }

  run_volume_tags = {
    Name = "fleeting-${source.name}"
  }

  temporary_iam_instance_profile_policy_document {
    Version = "2012-10-17"
    Statement {
      Action = [
        "ecr:GetDownloadUrlForLayer",
        "ecr:BatchGetImage",
        "ecr:CompleteLayerUpload",
        "ecr:BatchCheckLayerAvailability",
        "ecr:GetAuthorizationToken"
      ]
      Effect   = "Allow"
      Resource = ["*"]
    }
  }

  // It takes a while for the created image to settle down from `Pending` to `Available`.
  // It seems that with default settings for amazon ebs builder is not enough and packer
  // build call ends randomly with:
  //
  // Error waiting for AMI: Failed with ResourceNotReady error, which can have a variety of causes.
  //   For help troubleshooting, check our docs: https://www.packer.io/docs/builders/amazon.html#resourcenotready-error
  //   original error: ResourceNotReady: exceeded wait attempts
  //
  // Looking at the logs and creation time attached to the image, increasing the waiting
  // time from 10 minutes (40 * 15sec) to 120 minutes (120 * 60sec) will hopefully resolve
  // this problem.
  aws_polling {
    max_attempts  = 120
    delay_seconds = 60
  }
}

###############################
# AMI provisioning definition #
###############################

build {
  name = "fleeting"

  # add as many of these blocks as needed for other OSes
  source "source.amazon-ebs.macos" {
    name = "12-monterey-arm64"
  }

  provisioner "file" {
    source      = "./assets/nesting.plist"
    destination = "/Users/ec2-user/nesting.plist"
  }

  provisioner "file" {
    destination = "/Users/ec2-user/images.list"
    content     = <<EOT
%{for name, image in var.tart_images~}
${image} ${name}
%{endfor~}
EOT
  }

  provisioner "file" {
    destination = "/Users/ec2-user/nesting.checksum"
    content     = "${var.nesting.checksum}  nesting"
  }

  provisioner "shell" {
    scripts = [
      "./scripts/01_base_system_preparation.sh",
      "./scripts/02_docker_credentials_helper.sh",
      "./scripts/20_install_nesting.sh",
      "./scripts/30_install_tart.sh",
      "./scripts/31_test_tart_image_usage.sh",
      "./scripts/99_cleanup.sh",
    ]

    env = {
      NESTING_VERSION = var.nesting.version
    }
  }

  post-processor "manifest" {
    output     = "manifest.json"
    strip_path = true
  }
}
